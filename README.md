# Micro Benus: Spec Ops—A RW Mod 

Micro Benus: Spec Ops, or MB:SO, is a Rusted Warfare mod developed by Kyvex [1], and apart of our Micro Benus, including
standalone games and an older, unmaintained mod for Rusted Warfare.

## What does MB:SO add?

- **Two new factions** to play, as the *United Kingdom,* and the *Micro Benus* rebel group.

## Installation

1. To install MB:SO, you must first install Rusted Warfare, which can be found on [Steam](https://store.steampowered.com/app/647960/Rusted_Warfare__RTS/),
   [the Google Play Store](https://play.google.com/store/apps/details?id=com.corrodinggames.rts&hl=en_US&gl=US), or Apple App Store.
2. Once you have Rusted Warfare installed, you can install MB:SO by downloading the latest release from the [releases page](soon)
3. Once you have downloaded the latest release, you can install it by extracting the zip file to the Rusted Warfare mods folder.
4. Once you have extracted the zip file, you can enable the mod in the Rusted Warfare mods menu.

## Contributing

If you would like to contribute to MB:SO, you can do so by forking the repository, making your changes, and then
creating a pull request. This mod is an open source project, and we welcome any contributions to make it better.

## License

MB:SO for Rusted Warfare is licenced under the MIT licence, which can be found in the [LICENCE](LICENCE) file.